import { store } from '../src/store'

store.use(
  { count: 0 },
  {
    increment({ state }) {
      return { count: state.count + 1 }
    },
    decrement({ state }) {
      return { count: state.count - 1 }
    }
  }
)

const render = () => {
  const h1: any = document.querySelector('#count')
  h1.textContent = store.state.count
}

render()
store.on(_ => render())
;(window as any).store = store
