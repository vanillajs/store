[@vanillajs/store](../README.md) > ["store"](../modules/_store_.md)

# External module: "store"

## Index

### Type aliases

* [IActionHandler](_store_.md#iactionhandler)
* [IEventHandler](_store_.md#ieventhandler)

### Variables

* [_actions](_store_.md#_actions)
* [_listeners](_store_.md#_listeners)
* [_state](_store_.md#_state)

### Functions

* [emit](_store_.md#emit)

### Object literals

* [store](_store_.md#store)

---

## Type aliases

<a id="iactionhandler"></a>

###  IActionHandler

**Ƭ IActionHandler**: *`function`*

#### Type declaration
▸(state: *`any`*, payload?: *`any`*): `any`

**Parameters:**

| Name | Type |
| ------ | ------ |
| state | `any` |
| `Optional` payload | `any` |

**Returns:** `any`

___
<a id="ieventhandler"></a>

###  IEventHandler

**Ƭ IEventHandler**: *`function`*

#### Type declaration
▸(state: *`any`*, action: *`string`*): `any`

**Parameters:**

| Name | Type |
| ------ | ------ |
| state | `any` |
| action | `string` |

**Returns:** `any`

___

## Variables

<a id="_actions"></a>

### `<Let>` _actions

**● _actions**: *`Record`<`string`, [IActionHandler](_store_.md#iactionhandler)>*

___
<a id="_listeners"></a>

### `<Let>` _listeners

**● _listeners**: *[[IEventHandler](_store_.md#ieventhandler)] \| `any`* =  []

___
<a id="_state"></a>

### `<Let>` _state

**● _state**: *`any`*

___

## Functions

<a id="emit"></a>

###  emit

▸ **emit**(state: *`any`*, action: *`string`*): `void`

**Parameters:**

| Name | Type | Description |
| ------ | ------ | ------ |
| state | `any` |  Current store state |
| action | `string` |  name of action |

**Returns:** `void`

___

## Object literals

<a id="store"></a>

### `<Const>` store

**store**: *`object`*

A tiny, vanilla JS State Container

<a id="store.dispatch"></a>

####  dispatch

▸ **dispatch**(action: *`string`*, payload?: *`any`*): `any`

Dispatch an action with given payload. If action returns promise, dispatch will resolve after promise resolution. Same for async calls...

**Parameters:**

| Name | Type | Description |
| ------ | ------ | ------ |
| action | `string` |  action name |
| `Optional` payload | `any` |  data for action |

**Returns:** `any`

___
<a id="store.off"></a>

####  off

▸ **off**(handler: *[IEventHandler](_store_.md#ieventhandler)*): `void`

Unsubscribe from store updates

**Parameters:**

| Name | Type | Description |
| ------ | ------ | ------ |
| handler | [IEventHandler](_store_.md#ieventhandler) |  an event handler |

**Returns:** `void`

___
<a id="store.on"></a>

####  on

▸ **on**(handler: *[IEventHandler](_store_.md#ieventhandler)*): `function`

Subscribe to store updates

**Parameters:**

| Name | Type | Description |
| ------ | ------ | ------ |
| handler | [IEventHandler](_store_.md#ieventhandler) |  an event handler |

**Returns:** `function`

___
<a id="store.setstate"></a>

####  setState

▸ **setState**(state: *`any`*, action: *`string`*): `void`

Set state and call store listeners

**Parameters:**

| Name | Type | Description |
| ------ | ------ | ------ |
| state | `any` |  updated state |
| action | `string` |  action triggering state update |

**Returns:** `void`

___
<a id="store.use"></a>

####  use

▸ **use**(state: *`any`*, actions?: *`Record`<`string`, [IActionHandler](_store_.md#iactionhandler)>*): `void`

Register initial state and actions with store

**Parameters:**

| Name | Type | Description |
| ------ | ------ | ------ |
| state | `any` |  initial state |
| `Optional` actions | `Record`<`string`, [IActionHandler](_store_.md#iactionhandler)> |  list of actions |

**Returns:** `void`

___
<a id="store.state"></a>

####  state

**get state**(): `any`

Get Current State of the store

**Returns:** `any`

___

___

