const bundles = require('tslib-cli')
const isDev = !!process.env.ROLLUP_WATCH

let config = [
  {
    input: 'src/store.tsx',
    output: { file: 'dist/store.mjs', format: 'es' },
    tsconfigOverride: { exclude: ['node_modules', 'dist', 'public', 'test'] }
  },
  {
    input: 'src/store.tsx',
    output: { file: 'dist/store.js', format: 'umd', name: 'vanillajs' },
    tsconfigOverride: { exclude: ['node_modules', 'dist', 'public', 'test'] },
    minify: true
  }
]

// demo code on `npm start`
if (!!process.env.ROLLUP_WATCH) {
  config = [
    { input: 'public/index.tsx', output: { file: 'dist/index.js', format: 'umd', name: 'example' }, devServer: true }
  ]
}

export default bundles(config)
