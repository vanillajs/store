# @vanillajs/store

> A tiny, vanilla JS State Container

[![The GZIP size of @vanillajs/store](http://img.badgesize.io/https://unpkg.com/@vanillajs/store?compression=gzip&label=GZIP%20Size)](https://unpkg.com/@vanillajs/store)
[![The Brotli size of @vanillajs/store](http://img.badgesize.io/https://unpkg.com/@vanillajs/store?compression=brotli&label=Brotli%20Size)](https://unpkg.com/@vanillajs/store)

## Installation

Grab a copy from UNPKG CDN:

```html
<script src="https://unpkg.com/@vanillajs/store">
```

or Install it from NPM:

```bash
npm install @vanillajs/store
```

Then with a module bundler like [parcel](https://parceljs.org/) or [rollup](https://rollupjs.org/guide/en), use as you would anything else:

```js
import { store } from '@vanillajs/store'
```

## Usage

See [online demos](https://codesandbox.io/dashboard/sandboxes/@vanilla/store)

Use with Vanilla JS | See [codesandbox](https://codesandbox.io/s/zly4z2pq24)

Use with React | See [codesandbox](https://codesandbox.io/s/zr3y4o45vl)

Use with lit-html | See [codesandbox](https://codesandbox.io/s/ppyw7kj7rj)

## API

See the [docs directory](docs/modules/_store_.md)

## Contributing

**Found a problem?**

[Open an issue](https://gitlab.com/vanillajs/store/issues/new), or better yet [open a pull request](https://gitlab.com/vanillajs/store/merge_requests/new)

**Want to hack code?**

Clone this repo, and run `setup` script within repo root:

```bash
npm run setup
```

Once setup, review `examples` directory, or launch using `npm start` in any example directory.

## License

[MIT License](https://oss.ninja/mit/osdevisnot) @ [osdevisnot](https://github.com/osdevisnot)
